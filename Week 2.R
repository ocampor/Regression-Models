install.packages("UsingR")
library("UsingR")
# Loads library
data(dimond)

## Scatterplot with regression and confidence interval
library(ggplot2)
g = ggplot( diamond, aes( x = carat, y = price) )
g = g + xlab("Mass (carats)")
g = g + ylab("Price (SIN$)")
g = g + geom_point( size = 6, colour = "black", alpha = 0.2 )
g = g + geom_point( size = 5, colour = "blue", alpha = 0.2 )
g = g + geom_smooth( method = "lm", colour = "black" )
g

## Fitting the linear regression model
fit <- lm( price ~ carat, data = diamond )
coef( fit )

## Getting a more interpretable intercept
fit2 <- lm( price ~ I(carat - mean(carat) ), data = diamond )
coef(fit2)
###########################################################
# Thus $500.08 is the expected price for the average sized
# diamond of the data
###########################################################

## Predicting the price of a diamond
newx <- c(0.16, 0.27, 0.34)
coef(fit)[1] + coef(fit)[2] * newx
predict( fit, newdata = data.frame(carat = newx) )

## Predict all the training values
predict(fit)
